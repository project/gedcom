<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining gcattr type entities.
 */
interface GCAttrTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
