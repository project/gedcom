<?php
/**
 * Created by PhpStorm.
 * User: tilman
 * Date: 04.04.17
 * Time: 22:43
 */

namespace Drupal\gedcom\Entity;


use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\gedcom\GCDateParser;
use PhpGedcom\Gedcom;
use PhpGedcom\Record\Noteable;

class GC extends ContentEntityBase {

  public static function id2int(string $id) {
    return intval(substr($id, 1));
  }

  /**
   * @param $fields
   * @param $key
   * @param $label
   * @param $description
   * @param $weight
   *
   * @return BaseFieldDefinition
   */
  protected static function createDateField(&$fields, $key, $label, $description, &$weight) {
    $fields['gc_' . $key] = BaseFieldDefinition::create('datetime')
      ->setLabel($label)
      ->setDescription($description)
      ->setSetting('datetime_type', 'date')
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'weight' => $weight,
        'label' => 'inline',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'gedcom_date',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'date',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $weight++;
    return $fields['gc_' . $key];
  }

  /**
   * @param $fields
   * @param $key
   * @param $label
   * @param $description
   * @param $options
   * @param $weight
   *
   * @return BaseFieldDefinition
   */
  protected static function createOptionField(&$fields, $key, $label, $description, $options, &$weight) {
    $fields['gc_' . $key] = BaseFieldDefinition::create('list_string')
      ->setLabel($label)
      ->setDescription($description)
      ->setSettings([
        'allowed_values' => $options,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $weight++;
    return $fields['gc_' . $key];
  }

  /**
   * @param $fields
   * @param $key
   * @param $maxlen
   *
   * @param $label
   * @param $description
   * @param $weight
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   */
  protected static function createStringField(&$fields, $key, $maxlen, $label, $description, &$weight) {
    $fields['gc_' . $key] = BaseFieldDefinition::create('string')
      ->setLabel($label)
      ->setDescription($description)
      ->setSettings([
        'max_length' => $maxlen,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $weight++;
    return $fields['gc_' . $key];
  }

  /**
   * @param $fields
   * @param $key
   * @param $target_type
   * @param $label
   * @param $description
   * @param $weight
   * @param int $cardinality
   *
   * @return BaseFieldDefinition
   */
  protected static function createReferenceField(&$fields, $key, $target_type, $label, $description, &$weight, $cardinality = 1) {
    $fields['gc_' . $key] = BaseFieldDefinition::create('entity_reference')
      ->setLabel($label)
      ->setDescription($description)
      ->setRevisionable(FALSE)
      ->setSetting('target_type', $target_type)
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => $weight,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality($cardinality);
    $weight++;
    return $fields['gc_' . $key];
  }

  /**
   * @param $fields
   * @param $key
   * @param $target_type
   * @param $bundle
   * @param $label
   * @param $button
   * @param $description
   * @param $weight
   * @param int $cardinality
   *
   * @return BaseFieldDefinition
   */
  protected static function createBundleReferenceField(
    &$fields, $key, $target_type, $bundle, $label, $button, $description, $weight, $cardinality = 1
  ) {
    $fields['gc_' . $key] = BaseFieldDefinition::create('entity_reference')
      ->setLabel($label)
      ->setDescription($description)
      ->setRevisionable(FALSE)
      ->setSetting('target_type', $target_type)
      ->setSetting('bundle', $bundle)
      ->setSetting('handler', "default:$target_type")
      ->setSetting('handler_settings', [
        'target_bundles' => [
          $bundle => $bundle,
        ],
      ])
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_entity_view',
        'weight' => $weight,
        'settings' => [
          'view_mode' => 'default',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => $weight,
        'settings' => [
          'form_mode' => 'default',
          'label_singular' => $button,
          'label_plural' => '',
          'allow_new' => TRUE,
          'allow_existing' => FALSE,
          'match_operator' => 'CONTAINS',
          'override_labels' => TRUE,
        ],
        'region' => 'content',
        'third_party_settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality($cardinality);
    $weight++;
    return $fields['gc_' . $key];
  }

  /**
   * @param $fields
   * @param $key
   * @param $label
   * @param $description
   * @param $weight
   *
   * @return BaseFieldDefinition
   */
  protected static function createNoteField(&$fields, $key, $label, $description, &$weight) {
    $fields['gc_' . $key] = BaseFieldDefinition::create('string_long')
      ->setLabel($label)
      ->setDescription($description)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(-1);
    $weight++;
    return $fields['gc_' . $key];
  }

  /**
   * @param $fields
   * @param $bfd
   * @param $key
   * @param $label
   * @param $enable
   *
   * @return BaseFieldDefinition
   */
  protected static function cloneField(&$fields, $bfd, $key, $label, $show_form, $show_view) {
    $field = clone $bfd['gc_' . $key];
    $field->setLabel($label);
    if (!$show_form) {
      $field->setDisplayOptions('form', [
        'type' => 'hidden',
      ]);
    }
    if (!$show_view) {
      $field->setDisplayOptions('view', [
        'type' => 'hidden',
      ]);
    }
    $fields['gc_' . $key] = $field;
    return $field;
  }

  protected function clearGCAttr(array $keys) {
    $ids = [];
    foreach ($keys as $key) {
      foreach ($this->{'gc_' . $key} as $item) {
        $ids[] += $item->target_id;
      }
    }
    if (!empty($ids)) {
      $storage_handler = \Drupal::entityTypeManager()->getStorage('gcattr');
      $entities = $storage_handler->loadMultiple($ids);
      $storage_handler->delete($entities);
    }
  }



  protected function createGCAttr(string $key, $data, Gedcom $gedcom, $isfact = FALSE) {
    $eventRec = GCAttr::create([
      'type' => "gc$key",
    ]);
    if ($isfact) {
      $eventRec->set('gc_title', $data->getAttr());
    }
    $eventRec->set('gc_date_text', $data->getDate());
    $plac = $data->getPlac();
    $eventRec->set('gc_plac', $plac ? $plac->getPlac() : NULL);
    $eventRec->setNotes($data, $gedcom);
    $eventRec->save();
    return $eventRec;
  }

  protected function setFromEvent(string $key, $eventData, Gedcom $gedcom) {
    $attrRec = $this->createGCAttr($key, $eventData, $gedcom);
    $this->{'gc_' . $key} = $attrRec->id();
  }

  protected function addFromEvent(string $key, $eventData, Gedcom $gedcom) {
    $attrRec = $this->createGCAttr($key, $eventData, $gedcom);
    $this->gc_even[] = $attrRec->id();
  }

  protected function addFromFact(string $key, $factData, Gedcom $gedcom) {
    $attrRec = $this->createGCAttr($key, $factData, $gedcom, TRUE);
    $this->gc_fact[] = $attrRec->id();
  }

  /**
   * @param \PhpGedcom\Record\Noteable $rec
   * @param \PhpGedcom\Gedcom $gedcom
   */
  protected function setNotes(Noteable $rec, Gedcom $gedcom) {
    $this->gc_note = NULL;
    foreach ($rec->getNote() as $noteRef) {
      $note = $gedcom->getNote()[$noteRef->getNote()];
      if ($note) {
        $this->gc_note[] = $note->getNote();
      }
    }
  }

  protected function setChanged($srcRec, EntityChangedInterface $dst) {
    /** @var \PhpGedcom\Record\Chan $chan */
    $chan = $srcRec->getChan();
    if (!isset($chan)) {
      return;
    }
    $date_text = $chan->getDate();
    if (isset($date_text )) {
      $date = '';
      $code = GCDateParser::EMPTY;
      if (GCDateParser::getSingleDateInfo(strtoupper($date_text), $date, $code) == GCDateParser::SINGLE) {
        if ($code == GCDateParser::EXACT) {
          $dst->setChangedTime(strtotime($date));
        }
      }
    }
  }
}