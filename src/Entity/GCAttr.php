<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\gedcom\GCConfig;
use Drupal\gedcom\GCDateParser;
use Drupal\user\UserInterface;

/**
 * Defines the GEDCOM Event entity.
 *
 * @ingroup gedcom
 *
 * @ContentEntityType(
 *   id = "gcattr",
 *   label = @Translation("GEDCOM Attribute"),
 *   bundle_label = @Translation("GEDCOM Attribute type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gedcom\GCAttrListBuilder",
 *     "views_data" = "Drupal\gedcom\Entity\GCAttrViewsData",
 *     "form" = {
 *       "default" = "Drupal\gedcom\Form\GCAttrForm",
 *       "add" = "Drupal\gedcom\Form\GCAttrForm",
 *       "edit" = "Drupal\gedcom\Form\GCAttrForm",
 *       "delete" = "Drupal\gedcom\Form\GCAttrDeleteForm",
 *     },
 *     "access" = "Drupal\gedcom\GedcomAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gedcom\GCAttrHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gcattr",
 *   admin_permission = "administer gedcom",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "title_label" = "title_label",
 *     "date_label" = "date_label",
 *     "plac_label" = "plac_label",
 *   },
 *   links = {
 *     "canonical" = "/gedcom/gcattr/{gcattr}",
 *     "add-page" = "/gedcom/gcattr/add",
 *     "add-form" = "/gedcom/gcattr/add/{gcattr_type}",
 *     "edit-form" = "/gedcom/gcattr/{gcattr}/edit",
 *     "delete-form" = "/gedcom/gcattr/{gcattr}/delete",
 *     "collection" = "/admin/content/gedcom/gcattr",
 *   },
 *   bundle_entity_type = "gcattr_type",
 *   field_ui_base_route = "entity.gcattr_type.edit_form"
 * )
 */
class GCAttr extends GC implements GCAttrInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  public function preSave(EntityStorageInterface $storage) {
    $input = $this->gc_date_text->value;
    if (isset($input)) {
      $from_date = $to_date = '';
      $from_code = $to_code = GCDateParser::EMPTY;
      $result = GCDateParser::getFullDateInfo($input, $from_date, $from_code, $to_date, $to_code);

      $this->gc_date_from = NULL;
      $this->gc_date_to = NULL;
      if ($result != GCDateParser::INVALID) {
        if ($from_code == GCDateParser::EXACT) {
          $this->gc_date_from->setValue($from_date);
        }
        if ($to_code == GCDateParser::EXACT) {
          $this->gc_date_to->setValue($to_date);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public
  function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public
  function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public
  function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public
  function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public
  function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public
  function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public
  function label() {
    $label = '';
    $title = $this->gc_title->value;
    $date = $this->gc_date_text->value;
    $plac = $this->gc_plac->value;

    if ($title) {
      $label .= $title;
      if ($date || $plac) {
        $label .= '(';
      }
    }
    if ($date) {
      $label .= $date;
    }
    if ($date && $plac) {
      $label .= ' ';
    }
    if ($plac) {
      $label .= 'in ' . $plac;
    }
    if ($title && ($date || $plac)) {
      $label .= ')';
    }

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public
  static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $bfd) {
    $fields = [];

    $type_desc = GCAttrType::load($bundle);
    $all_attrs = GCConfig::getAllAttrConfig();
    $cur_attr = $all_attrs[substr($bundle, 2)];

    if ($type_desc || $cur_attr) {
      $item_lc = isset($bundle_desc['item_lc']) ? $bundle_desc['item_lc'] : $bundle;
      $item_uc = isset($bundle_desc['item_uc']) ? $bundle_desc['item_uc'] : strtoupper($bundle);

      $default_desc['title_label'] = t('@item_uc', ['@item_uc' => $item_uc], ['context' => 'gedcom']);
      $default_desc['date_gc_label'] = t('@item_uc: GEDCOM Date', ['@item_uc' => $item_uc], ['context' => 'gedcom']);
      $default_desc['date_from_label'] = t('@item_uc: From', ['@item_uc' => $item_uc], ['context' => 'gedcom']);
      $default_desc['date_to_label'] = t('@item_uc: Until', ['@item_uc' => $item_uc], ['context' => 'gedcom']);
      $default_desc['plac_label'] = t('Place of @item_lc', ['@item_lc' => $item_lc], ['context' => 'gedcom']);
      $cur_attr += $default_desc;

      $enable_title = $type_desc ? $type_desc->get('show_title') : '';
      $title_label = $type_desc ? $type_desc->get('title_label') : '';
      $title_label = empty($title_label) ? $cur_attr['title_label'] : $title_label;

      $enable_date = $type_desc ? $type_desc->get('show_date') : '';
      $date_gc_label = $type_desc ? $type_desc->get('date_gc_label') : '';
      $date_gc_label = empty($date_gc_label) ? $cur_attr['date_gc_label'] : $date_gc_label;

      $date_from_label = $type_desc ? $type_desc->get('date_from_label') : '';
      $date_from_label = empty($date_from_label) ? $cur_attr['date_from_label'] : $date_from_label;

      $enable_todate = $type_desc ? $type_desc->get('show_todate') : '';
      $date_to_label = $type_desc ? $type_desc->get('date_to_label') : '';
      $date_to_label = empty($date_to_label) ? $cur_attr['date_to_label'] : $date_to_label;

      $enable_plac = $type_desc ? $type_desc->get('show_plac') : '';
      $plac_label = $type_desc ? $type_desc->get('plac_label') : '';
      $plac_label = empty($plac_label) ? $cur_attr['plac_label'] : $plac_label;

      $field = GC::cloneField($fields, $bfd, 'title', $title_label, $enable_title, $enable_title);
      if (isset($cur_attr['size'])) {
        $field->setSettings([
          'max_length' => $cur_attr['size'],
          'text_processing' => 0,
        ]);
      }
      GC::cloneField($fields, $bfd, 'date_text', $date_gc_label, $enable_date, FALSE);
      GC::cloneField($fields, $bfd, 'date_from', $date_from_label, FALSE, $enable_date);
      GC::cloneField($fields, $bfd, 'date_to', $date_to_label, FALSE, $enable_todate);
      GC::cloneField($fields, $bfd, 'plac', $plac_label, $enable_plac, $enable_plac);

      if (isset($cur_attr['date_format'])) {
        foreach (['gc_date_from', 'gc_date_to'] as $field_key) {
          $field = $fields[$field_key];
          $fdo = $field->getDisplayOptions('view');
          $fdo['settings']['format_type'] = $cur_attr['date_format'];
          $field->setDisplayOptions('view', $fdo);
        }
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public
  static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the record.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the record was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the record was last edited.'));

    $weight = 10;
    GC::createStringField($fields, 'title', 248, // = max size used in facts/events
      t('Event', [], ['context' => 'gedcom']), '', $weight);

    GC::createStringField($fields, 'date_text', 35,
      t('Date', [], ['context' => 'gedcom']),
      t('Date in GEDCOM format'), $weight)
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ]);

    GC::createDateField($fields, 'date_from',
      t('Date (from)', [], ['context' => 'gedcom']),
      t('First part of GEDCOM date if it can be mathed to a particular date'), $weight)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    GC::createDateField($fields, 'date_to',
      t('Date (to)', [], ['context' => 'gedcom']),
      t('Second part of GEDCOM date if it can be mathed to a particular date'), $weight)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    GC::createStringField($fields, 'plac', 120,
      t('Place', [], ['context' => 'gedcom']), '', $weight);

    GC::createNoteField($fields, 'note',
      t('Note', [], ['context' => 'gedcom']),
      t('Notes attached to FAM records'), $weight);

    return $fields;
  }

  public
  static function createBundle($key, array $bundle_desc, array $default_desc) {
    $bundle_desc += $default_desc;
    //  Clear
    $bundle_desc['title_label'] = '';
    $bundle_desc['date_gc_label'] = '';
    $bundle_desc['date_from_label'] = '';
    $bundle_desc['date_to_label'] = '';
    $bundle_desc['plac_label'] = '';

    $bundle_desc['langcode'] = 'en';
    $bundle_desc['status'] = TRUE;
    $bundle_desc['id'] = "gc$key";

    $event_bundle = GCAttrType::create($bundle_desc);
    $event_bundle->save();
    return $event_bundle;
  }
}
