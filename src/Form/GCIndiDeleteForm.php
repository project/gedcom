<?php

namespace Drupal\gedcom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Individual records.
 *
 * @internal
 */
class GCIndiDeleteForm extends ContentEntityDeleteForm {

}
