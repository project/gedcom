<?php

namespace Drupal\gedcom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting gcattr entities.
 *
 * @internal
 */
class GCAttrDeleteForm extends ContentEntityDeleteForm {

}
