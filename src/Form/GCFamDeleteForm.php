<?php

namespace Drupal\gedcom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Family records.
 *
 * @internal
 */
class GCFamDeleteForm extends ContentEntityDeleteForm {

}
