<?php
/**
 * Created by PhpStorm.
 * User: tilman
 * Date: 04.07.17
 * Time: 23:46
 */

namespace Drupal\gedcom;


class GCDateParser {

  const DATE_RANGE = "/^(BEF (.+)|AFT (.+)|BET (.+) AND (.+))$/";

  const DATE_PERIOD = "/^(FROM (.+)|TO (.+)|FROM (.+) TO (.+))$/";

  const DATE_APPROX = "/^(ABT (.+)|CAL (.+)|EST (.+))$/";

  const DATE_GREG = "/^(((\\d{1,2})\\s+)?(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\\s+)?(\\d+)$/";

  const MONTH2IDX = [
    'JAN' => '01',
    'FEB' => '02',
    'MAR' => '03',
    'APR' => '04',
    'MAY' => '05',
    'JUN' => '06',
    'JUL' => '07',
    'AUG' => '08',
    'SEP' => '09',
    'OCT' => '10',
    'NOV' => '11',
    'DEC' => '12',
  ];

  public static function getDateString(string $date) {
    if (preg_match(self::DATE_GREG, $date, $match) == 1) {
      if (!empty($match[3])) {
        return sprintf("%'04d-%'02d-%'02d",
          $match[5], self::MONTH2IDX[$match[4]], $match[3]);
      }
      elseif (!empty($match[4])) {
        return sprintf('%04d-%02d-01',
          $match[5], self::MONTH2IDX[$match[4]]);
      }
      elseif (!empty($match[5])) {
        return sprintf('%04d-01-01',
          $match[5]);
      }
    }
    return FALSE;
  }

  public static function isValidDate(string $date): bool {
    return (preg_match(self::DATE_GREG, $date) == 1);
  }

  public static function isExactDate(string $date): bool {
    if (preg_match(self::DATE_GREG, $date, $match) == 1) {
      return !empty($match[3]);
    }
  }

  public static function isValidDateString(string $date) {
    if (self::isValidDate($date)) {
      return TRUE;
    }
    elseif (preg_match(self::DATE_RANGE, $date, $match) == 1) {
      switch (count($match)) {
        case 3:
          return self::isValidDate($match[2]);
        case 4:
          return self::isValidDate($match[3]);
        case 6:
          $from = self::getDateString($match[4]);
          $to = self::getDateString($match[5]);
          return $from && $to && ($from < $to);
        default:
          return FALSE;
      }
    }
    elseif (preg_match(self::DATE_PERIOD, $date, $match) == 1) {
      switch (count($match)) {
        case 3:
          return self::isValidDate($match[2]);
        case 4:
          return self::isValidDate($match[3]);
        case 6:
          return self::isValidDate($match[4]) && self::isValidDate($match[5]);
        default:
          return FALSE;
      }
    }
    elseif (preg_match(self::DATE_APPROX, $date, $match) == 1) {
      switch (count($match)) {
        case 3:
          return self::isValidDate($match[2]);
        case 4:
          return self::isValidDate($match[3]);
        case 5:
          return self::isValidDate($match[4]);
        default:
          return FALSE;
      }
    }
    return FALSE;
  }

  const INVALID = 0;
  const SINGLE = 1;
  const APPROX = 2;
  const RANGE = 3;
  const PERIOD = 4;

  const EMPTY = 0;
  const INCOMPLETE = 1;
  const EXACT = 2;



  /**
   * @param string $input
   * @param $match
   *
   * @return int
   */
  public static function getSingleDateInfo(string $input,
                                              string &$date,
                                              int &$code) {
    if (preg_match(self::DATE_GREG, $input, $match) == 1) {
      if (!empty($match[3])) {
        $date = sprintf("%'04d-%'02d-%'02d",
          $match[5], self::MONTH2IDX[$match[4]], $match[3]);
        $code = self::EXACT;
        return self::SINGLE;
      }
      elseif (!empty($match[4])) {
        $date = sprintf('%04d-%02d-01',
          $match[5], self::MONTH2IDX[$match[4]]);
        $code = self::INCOMPLETE;
        return self::SINGLE;
      }
      elseif (!empty($match[5])) {
        $date = sprintf('%04d-01-01',
          $match[5]);
        $code = self::INCOMPLETE;
        return self::SINGLE;
      }
    }
    $date = '';
    $code = self::EMPTY;
    return self::INVALID;
  }

  public static function getFullDateInfo(string $input,
                                         string &$from_date, int &$from_code,
                                         string &$to_date, &$to_code) {
    if (self::getSingleDateInfo($input, $from_date, $from_code) == self::SINGLE) {
      $to_date = '';
      $to_code = self::EMPTY;
      return self::SINGLE;
    }
    elseif (preg_match(self::DATE_RANGE, $input, $match) == 1) {
      switch (count($match)) {
        case 3:
          if (self::getSingleDateInfo($match[2], $from_date, $from_code) == self::SINGLE)
          {
            $to_date = '';
            $to_code = self::EMPTY;
            return self::RANGE;
          }
          break;
        case 4:
          if (self::getSingleDateInfo($match[3], $to_date, $to_code) == self::SINGLE)
          {
            $from_date = '';
            $from_code = self::EMPTY;
            return self::RANGE;
          }
          break;
        case 6:
          if (self::getSingleDateInfo($match[4], $from_date, $from_code) == self::SINGLE) {
            if (self::getSingleDateInfo($match[5], $to_date, $to_code) == self::SINGLE) {
              if ($from_date < $to_date) {
                return self::RANGE;
              } else {
                return self::INVALID; //TODO: range mismatch
              }
            }
          }
          break;
      }
    }
    elseif (preg_match(self::DATE_PERIOD, $input, $match) == 1) {
      switch (count($match)) {
        case 3:
          if (self::getSingleDateInfo($match[2], $from_date, $from_code) == self::SINGLE)
          {
            $to_date = '';
            $to_code = self::EMPTY;
            return self::PERIOD;
          }
          break;
        case 4:
          if (self::getSingleDateInfo($match[3], $to_date, $to_code) == self::SINGLE)
          {
            $from_date = '';
            $from_code = self::EMPTY;
            return self::PERIOD;
          }
          break;
        case 6:
          if (self::getSingleDateInfo($match[4], $from_date, $from_code) == self::SINGLE) {
            if (self::getSingleDateInfo($match[5], $to_date, $to_code) == self::SINGLE) {
              if ($from_date < $to_date) {
                return self::PERIOD;
              } else {
                return self::INVALID; //TODO: period mismatch
              }
            }
          }
          break;
      }
    }
    elseif (preg_match(self::DATE_APPROX, $input, $match) == 1) {
      switch (count($match)) {
        case 3:
          if (self::getSingleDateInfo($match[2], $from_date, $from_code) == self::SINGLE)
          {
            $to_date = '';
            $to_code = self::EMPTY;
            return self::APPROX;
          }
          break;
        case 4:
          if (self::getSingleDateInfo($match[3], $from_date, $from_code) == self::SINGLE)
          {
            $to_date = '';
            $to_code = self::EMPTY;
            return self::APPROX;
          }
          break;
        case 5:
          if (self::getSingleDateInfo($match[4], $from_date, $from_code) == self::SINGLE)
          {
            $to_date = '';
            $to_code = self::EMPTY;
            return self::APPROX;
          }
          break;
      }
    }
    $from_date = '';
    $from_code = self::EMPTY;
    $to_date = '';
    $to_code = self::EMPTY;
    return self::INVALID;
  }

  /*    Top Down Grammar of GEDCOM Date values

  DATE_VALUE:
  [
    <DATE_RANGE>
    <DATE_PERIOD> |
    <DATE_APPROXIMATED> |
    <DATE> |
    INT <DATE> (<DATE_PHRASE>) |
    (<DATE_PHRASE>)
  ]

  DATE_RANGE:
  [
    BEF <DATE> |
    AFT <DATE> |
    BET <DATE> AND <DATE>
  ]

  DATE_PERIOD:
  [
    FROM <DATE> |
    TO <DATE> |
    FROM <DATE> TO <DATE>
  ]

  DATE_APPROXIMATED:
  [
    ABT <DATE> |
    CAL <DATE> |
    EST <DATE>
  ]

  DATE:
    [ <DATE_CALENDAR_ESCAPE> | <NULL>]
    <DATE_CALENDAR>

  DATE_CALENDAR_ESCAPE:
    [ @#DHEBREW@ | @#DROMAN@ | @#DFRENCH R@ | @#DGREGORIAN@ |
    @#DJULIAN@ | @#DUNKNOWN@ ]

  DATE_CALENDAR: = {Size=4:35}
    [ <DATE_GREG> | <DATE_JULN> | <DATE_HEBR> | <DATE_FREN> |
    <DATE_FUTURE> ]

  DATE_GREG:
    [ <YEAR_GREG> | <MONTH> <YEAR_GREG> | <DAY> <MONTH> <YEAR_GREG> ]

  DATE_JULN:
    [ <YEAR> | <MONTH> <YEAR> | <DAY> <MONTH> <YEAR> ]

  DATE_HEBR:
    [ <YEAR> | <MONTH_HEBR> <YEAR> | <DAY> <MONTH_HEBR> <YEAR> ]

  DATE_FREN:
    [ <YEAR> | <MONTH_FREN> <YEAR> | <DAY> <MONTH_FREN> <YEAR> ]

  DATE_EXACT:
    <DAY> <MONTH> <YEAR_GREG>


  DATE_LDS_ORD:
    <DATE_VALUE>


  DATE_PHRASE:
    (<TEXT>)

  DAY:
    dd

  MONTH:
    [ JAN | FEB | MAR | APR | MAY | JUN |
    JUL | AUG | SEP | OCT | NOV | DEC ]

  MONTH_FREN:
    [ VEND | BRUM | FRIM | NIVO | PLUV | VENT | GERM |
    FLOR | PRAI | MESS | THER | FRUC | COMP ]

  MONTH_HEBR:
    [ TSH | CSH | KSL | TVT | SHV | ADR | ADS |
    NSN | IYR | SVN | TMZ | AAV | ELL ]

  YEAR:

  YEAR_GREG:
    [ <NUMBER> | <NUMBER>/<DIGIT><DIGIT> ]

  */


}